# Copyright (c) 2023, Pascal Volk
# See COPYING for distribution information.
"""
    VirtualMailManager.ext.lib.crypt
    --------------------------------

    Virtual Mail Manager's  `connection' to system's crypt(3) library.
"""
import ctypes

from os import strerror

from ...import ENCODING
from .import SystemLibraryError


__all__ = ('crypt',)


def _setup_libcrypt_crypt():
    """Explain ctypes what the following signature could mean:

           char *crypt(const char *phrase, const char *setting)
    """
    libcrypt = ctypes.CDLL('libcrypt.so.1', use_errno=True)
    crypt = libcrypt.crypt
    crypt.argtypes = [ctypes.c_char_p, ctypes.c_char_p]
    crypt.restype = ctypes.c_char_p

    return crypt


_crypt = _setup_libcrypt_crypt()


def crypt(password, salt):
    """Returns a string representing hashed/crypt()-ed *password*.

    Arguments:

        `password` : string
            The clear text password
        `salt` : string
            The  hashing method to use
    """
    password = ctypes.c_char_p(bytes(password, encoding=ENCODING))
    salt = ctypes.c_char_p(bytes(salt, encoding=ENCODING))

    ctypes.set_errno(0)
    hashed = ctypes.c_char_p(_crypt(password, salt))
    errno = ctypes.get_errno()

    if not errno:
        return hashed.value.decode()
    else:
        raise SystemLibraryError(strerror(errno), errno)
