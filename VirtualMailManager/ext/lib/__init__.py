# Copyright (c) 2023, Pascal Volk
# See COPYING for distribution information.
"""
    VirtualMailManager.ext.lib
    --------------------------

 VMM's little helpers which work with foreign functions from system libraries.
"""

from ...errors import VMMError


class SystemLibraryError(VMMError):
    """Exception class for errors from foreign functions."""
    pass
