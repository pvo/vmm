#!/usr/bin/env python3
# Copyright 2007 - 2023, Pascal Volk
# See COPYING for distribution information.

from setuptools import setup
from setuptools.command.install import install


long_description = """
vmm - a virtual mail manager.
vmm is the easy to use and configurable command line tool for administrators
and postmasters, to manage domains, alias-domains, accounts and relocated
mail users. It allows the fast and easy management of mail servers.

vmm is written in Python. It’s designed for installations using Dovecot and
Postfix with a PostgreSQL backend. vmm should work on each Linux/UNIX-like
operation system.
"""
packages = [
    'VirtualMailManager',
    'VirtualMailManager.cli',
    'VirtualMailManager.ext',
    'VirtualMailManager.ext.lib',
]

# sucessfuly tested on:
platforms = ['freebsd7', 'linux2', 'openbsd5']

setup_args = {'long_description': long_description,
              'long_description_content_type' : 'text/plain',
              'packages': packages,
              'download_url': 'https://vmm.localdomain.org/download.html',
              'platforms': platforms}

setup(**setup_args)
