Installation Prerequisites
You should already have installed and configured Postfix, Dovecot ≥ 2.0
and PostgreSQL.

The Virtual Mail Manager depends on:
    - Python (≥ 3.2)
    - Psycopg¹ (≥ 2.0)

[1] Psycopg: <https://www.psycopg.org/> (Debian: python3-psycopg2)


Create additionally a user and groups for improved security
  We will create the system user `doveauth'. This user is used in the
  authentication process. On a Debian GNU/Linux System use this command:

	adduser --system --home /nonexistent --no-create-home --group \
	--disabled-login --gecos "Dovecot IMAP/POP3 authentication user" \
	doveauth

  This will create the doveauth user and group.
  For Dovecot ≥ 2.0 we create also the group `dovemail'. Dovecot will assign
  this group to all Dovecot processes.
  On a Debian GNU/Linux bases system run:

	addgroup --system dovemail


Configuring PostgreSQL
(for more details see:
    https://vmm.localdomain.org/installation/postgresql_configuration.html)

* /etc/postgresql/13/main/pg_hba.conf
  [ if you prefer to connect via TCP/IP ]
    # IPv4 local connections:
    host    mailsys     +mailsys    127.0.0.1/32          md5
  [ if you want to connect through a local Unix-domain socket ]
    # "local" is for Unix domain socket connections only
    local   mailsys     +mailsys                          md5

    # reload configuration
    systemctl reload postgresql@13-main.service

* Create a database superuser if necessary:
    # as root run: su - postgres
    # if you have sudo privileges run: sudo su - postgres
    # create your superuser, which will be able to create users and databases
    createuser -s -d -r -E -e -P $USERNAME

* As superuser create the database and db users for vmm, Postfix and Dovecot
    connecting to PostgreSQL:
    psql template1

    # create users, group and the database
    CREATE ROLE vmm LOGIN ENCRYPTED PASSWORD 'DB PASSWORD for vmm';
    CREATE ROLE dovecot LOGIN ENCRYPTED password 'DB PASSWORD for Dovecot';
    CREATE ROLE postfix LOGIN ENCRYPTED password 'DB PASSWORD for Postfix';
    CREATE ROLE mailsys WITH USER postfix, dovecot, vmm;
    CREATE DATABASE mailsys WITH OWNER vmm ENCODING 'UTF8';
    \q

    # connect to the new database
    psql mailsys vmm -W -h 127.0.0.1
    # import the database structure
    \i vmm-x.y.z/pgsql/create_tables.pgsql
    # leave psql
    \q

    # set permissions for your Dovecot and Postfix users
    # see python set-permissions.py -h for details
    python vmm-x.y.z/pgsql/set-permissions.py -a -H 127.0.0.1 -U vmm

Create directory for your mails
  mkdir /srv/mail
  cd /srv/mail/
  mkdir 0 1 2 3 4 5 6 7 8 9 a b c d e f g h i j k l m n o p q r s t u v w x y z
  chmod 771 /srv/mail
  chmod 751 /srv/mail/*


For Dovecot's configuration read the file Configure.Dovecot_2


Configuring Postfix's main.cf
    sql      = pgsql:${config_directory}/
    proxysql = proxy:${sql}

    # relocated users from the database
    #relocated_maps = ${proxysql}pgsql-relocated_maps.cf

    # transport settings from our database
    transport_maps = ${proxysql}pgsql-transport_maps.cf

    # virtual domains
    virtual_mailbox_domains = ${proxysql}pgsql-virtual_mailbox_domains.cf
    virtual_alias_maps = ${proxysql}pgsql-virtual_alias_maps.cf
    virtual_minimum_uid = 70000
    virtual_uid_maps = ${sql}pgsql-virtual_uid_maps.cf
    virtual_gid_maps = ${sql}pgsql-virtual_gid_maps.cf
    virtual_mailbox_base = /
    virtual_mailbox_maps = ${proxysql}pgsql-virtual_mailbox_maps.cf

    # dovecot lmtp
    virtual_transport = lmtp:unix:private/dovecot-lmtp

    # dovecot SASL
    smtpd_sasl_type = dovecot
    smtpd_sasl_path = private/dovecot-auth
    smtpd_sasl_auth_enable = yes
    # Keep smtpd_sasl_local_domain identical to Dovecot's auth_default_realm:
    # empty. Both are empty by default. Let it commented out.
    # Read more at: http://wiki.dovecot.org/Authentication/Mechanisms/DigestMD5
    #smtpd_sasl_local_domain =
    smtpd_sasl_security_options = noplaintext, noanonymous
    #smtpd_sasl_security_options = noanonymous
    #broken_sasl_auth_clients = yes

    smtpd_recipient_restrictions =
      permit_mynetworks
      permit_sasl_authenticated
      reject_unauth_destination


Installing the Virtual Mail Manager and configure the rest

    Installing from Git or vmm-x.y.z.tar.gz
    after cloning from the Git repo or extracting the archive change into the
    new directory and type:
        ./install.sh
    edit all the pgsql-*.cf files in /etc/postfix

    reload postfix

    # configure the Virtual Mail Manager
    # vmm.cfg(5) - configuration file for vmm
    #
    # For Dovecot v2.x use 'lmtp:unix:private/dovecot-lmtp' as
    # domain.transport
    vmm configure

    # for help type
    # vmm(1) - command line tool to manage email domains/accounts/aliases
    vmm --help
