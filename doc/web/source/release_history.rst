======================
Release history of vmm
======================

=========== ============ ===========================================================
Version     Release Date Download URL
=========== ============ ===========================================================
vmm-0.7.0   2021-11-07   :dl_link:`vmm-0.7.0.tar.gz`/:dl_link:`vmm-0.7.0.tar.gz.sig`
vmm-0.6.2   2014-02-01   :dl_link:`vmm-0.6.2.tar.gz`/:dl_link:`vmm-0.6.2.tar.gz.sig`
vmm-0.6.1   2012-10-03   :dl_link:`vmm-0.6.1.tar.gz`/:dl_link:`vmm-0.6.1.tar.gz.sig`
vmm-0.6.0   2012-06-28   :dl_link:`vmm-0.6.0.tar.gz`/:dl_link:`vmm-0.6.0.tar.gz.sig`
vmm-0.5.2   2009-09-09   :dl_link:`vmm-0.5.2.tar.gz`/:dl_link:`vmm-0.5.2.tar.gz.sig`
vmm-0.5.1   2009-08-12   :dl_link:`vmm-0.5.1.tar.gz`/:dl_link:`vmm-0.5.1.tar.gz.sig`
vmm-0.5     2008-11-27   :dl_link:`vmm-0.5.tar.gz`/:dl_link:`vmm-0.5.tar.gz.sig`
vmm-0.4-r41 2008-05-02   :dl_link:`vmm-0.4-r41.tar.bz2`
vmm-0.4     2008-05-01   :dl_link:`vmm-0.4.tar.bz2`
vmm-0.3.1   2008-01-08   :dl_link:`vmm-0.3.1.tar.bz2`
vmm-0.3     2008-01-06   :dl_link:`vmm-0.3.tar.bz2`
=========== ============ ===========================================================
